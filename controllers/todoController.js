const TodoModel = require('../model/todoModel');

module.exports.createTodo = async (req, res, next) => {
  try {
    const newTodo = await TodoModel.create(req.body);
    res.status(201).json(newTodo);
  } catch (err) {
    next(err);
  }
};

module.exports.getTodos = async (_req, res, next) => {
  try {
    const todoList = await TodoModel.find({});
    res.status(201).json(todoList);
  } catch (err) {
    next(err);
  }
};

module.exports.getTodoById = async (req, res, next) => {
  try {
    const { id } = req.params;
    const todo = await TodoModel.findById(id);

    if (todo) {
      res.status(200).json(todo);
    } else {
      res.status(404).json({
        message: 'Note not found.'
      });
    }
  } catch (err) {
    next(err);
  }
};

module.exports.updateTodo = async (req, res, next) => {
  try {
    const { params: { id }, body } = req;
    const updatedTodo = await TodoModel.findByIdAndUpdate(id, body, { new: true, useFindAndModify: false });

    if (updatedTodo) {
      res.status(200).json(updatedTodo);
    } else {
      res.status(404).json({
        message: 'Note not found.'
      });
    }
  } catch (err) {
    next(err);
  }
};

module.exports.deleteTodo = async (req, res, next) => {

  try {
    const { id } = req.params;
    const deletedTodo = await TodoModel.findByIdAndDelete(id);
    if (deletedTodo) {
      res.status(200).json(deletedTodo);
    } else {
      res.status(404).json({
        message: 'Note not found'
      });
    }
  } catch (err) {
    next(err);
  }

};
