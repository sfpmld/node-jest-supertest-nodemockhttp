const httpMocks = require('node-mocks-http');
const TodoController = require('../../controllers/todoController');
const TodoModel = require('../../model/todoModel');
// TodoModel.create = jest.fn();
// TodoModel.find = jest.fn();
// TodoModel.findById = jest.fn();
// TodoModel.findByIdAndUpdate = jest.fn();
// jest.mock('../../model/todoModel');
jest.mock('../../model/todoModel');

const newTodo = require('../mock-data/new-todo.json');
const allTodos = require('../mock-data/all-todos.json');
const updatedTodo = require('../mock-data/updated-todo.json');
const unknownError = {
  message: 'Something bad happened'
};

let req, res, next;
const mockId = '5e964a5f14cf6b502c691c0f';

beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = jest.fn();
});

describe('TodoController.getTodo', () => {
  it('should have to getTodo function', async () => {
    expect(typeof TodoController.getTodos).toEqual('function');
  });

  it('should call TodoModel.find', async () => {
    await TodoController.getTodos(req, res, next);
    expect(TodoModel.find).toHaveBeenCalledWith({});
  });

  it('should return response with status 200 and all todos', async () => {
    TodoModel.find.mockReturnValue(allTodos);
    await TodoController.getTodos(req, res, next);
    expect(res.statusCode).toEqual(201);
    expect(res._isEndCalled).toBeTruthy();
    expect(res._getJSONData()).toEqual(allTodos);
  });

  it('should handle errors in getTodos', async () => {
    const rejectedPromise = Promise.reject(unknownError);
    TodoModel.find.mockReturnValue(rejectedPromise);
    await TodoController.getTodos(req, res, next);
    expect(next).toHaveBeenCalledWith(unknownError);
  });
});

describe('TodoController.getTodoById', () => {

  beforeEach(async () => {
    req.params.id = mockId;
  });

  it('should have a getTodoById method', async () => {
    expect(typeof TodoController.getTodoById).toEqual('function');
  });

  it('should call TodoModel.findById with route parameters', async () => {
    await TodoController.getTodoById(req, res, next);
    expect(TodoModel.findById).toHaveBeenCalledWith(mockId);
  });

  it('should return a json body and response code 200', async () => {
    TodoModel.findById.mockReturnValue({
      mockId,
      ...newTodo
    });
    await TodoController.getTodoById(req, res, next);
    expect(res.statusCode).toEqual(200);
    expect(res._getJSONData()).toEqual({
      mockId,
      ...newTodo
    });
    expect(res._isEndCalled()).toBeTruthy();
  });

  it('should do error handling', async () => {
    const mockResponse = Promise.reject(unknownError);
    TodoModel.findById.mockReturnValue(mockResponse);

    await TodoController.getTodoById(req, res, next);
    expect(next).toHaveBeenCalledWith(unknownError);
  });

  it('should return 404 when item not found', async () => {
    const responseMocked = {
      message: 'Note not found.',
    };
    TodoModel.findById.mockReturnValue(null);

    await TodoController.getTodoById(req, res, next);
    expect(res.statusCode).toBe(404);
    expect(res._getJSONData()).toEqual(responseMocked);
    expect(res._isEndCalled()).toBeTruthy();
  });
});

describe('TodoController.createTodo', () => {
  beforeEach(() => {
    req.body = newTodo;
  });

  it('should have a createTodo function', async () => {
    expect(typeof TodoController.createTodo).toEqual('function');
  });

  it('should call TodoModel.create', async () => {
    await TodoController.createTodo(req, res, next);
    expect(TodoModel.create).toHaveBeenCalledWith(newTodo);
  });

  it('should return 201 response code', async () => {
    await TodoController.createTodo(req, res, next);
    expect(res.statusCode).toBe(201);
    expect(res._isEndCalled()).toBeTruthy();
  });

  it('should return json body in the response', async () => {
    TodoModel.create.mockReturnValue({
      mockId,
      ...newTodo
    });
    await TodoController.createTodo(req, res, next);
    expect(res._getJSONData()).toStrictEqual({
      mockId,
      ...newTodo
    });
  });

  it('should handle errors', async () => {
    const errorCreateTodo = { message: 'Done property missing' };
    const rejectedPromise = Promise.reject(errorCreateTodo);
    TodoModel.create.mockReturnValue(rejectedPromise);
    await TodoController.createTodo(req, res, next);
    expect(next).toHaveBeenCalledWith(errorCreateTodo);
  });
});

describe('TodoController.udpateTodo', () => {
  beforeEach(async () => {
    req.body = updatedTodo;
    req.params.id = mockId;
  });

  it('should have a function TodoController.updateTodo', async () => {
    expect(typeof TodoController.updateTodo).toEqual('function');
  });

  it('should update with TodoModel.findByIdAndUpdate method', async () => {
    await TodoController.updateTodo(req, res, next);
    expect(TodoModel.findByIdAndUpdate).toHaveBeenCalledWith(mockId, updatedTodo, { new: true, useFindAndModify: false });
  });

  it('should return a response with the json data and http code 200', async () => {
    TodoModel.findByIdAndUpdate.mockReturnValue({
      mockId,
      ...updatedTodo
    });
    await TodoController.updateTodo(req, res, next);
    expect(res._isEndCalled).toBeTruthy();
    expect(res.statusCode).toEqual(200);
    expect(res._getJSONData()).toEqual({
      mockId,
      ...updatedTodo
    });

  });

  it('should handle errors', async () => {
    const rejectedPromise = Promise.reject(unknownError);

    TodoModel.findByIdAndUpdate.mockReturnValue(rejectedPromise);
    await TodoController.updateTodo(req, res, next);

    expect(next).toHaveBeenCalledWith(unknownError);
  });

  it('should return a 404 error code when note doesn\'t exists', async () => {
    TodoModel.findByIdAndUpdate.mockReturnValue(null);
    await TodoController.updateTodo(req, res, next);

    expect(res._isEndCalled).toBeTruthy();
    expect(res.statusCode).toBe(404);
  });
});

describe('TodoController.deleteTodo', () => {
  beforeEach(async () => {
    req.params.id = mockId;
  });

  it('should have a TodoController.deleteTodo', async () => {
    expect(typeof TodoController.deleteTodo).toEqual('function');
  });

  it('should call TodoModel.findByIdAndDelete', async () => {
    await TodoController.deleteTodo(req, res, next);
    expect(TodoModel.findByIdAndDelete).toHaveBeenCalledWith(mockId);
  });

  it('should return a 200 status code when deleting todo', async () => {
    TodoModel.findByIdAndDelete.mockReturnValue({
      mockId,
      ...newTodo
    });
    await TodoController.deleteTodo(req, res, next);

    expect(res._isEndCalled()).toBeTruthy();
    expect(res.statusCode).toBe(200);
    expect(res._getJSONData()).toEqual({
      mockId,
      ...newTodo
    });
  });

  it('should return a 404 status code when not not found', async () => {
    TodoModel.findByIdAndDelete.mockReturnValue(null);
    await TodoController.deleteTodo(req, res, next);

    expect(res._isEndCalled()).toBeTruthy();
    expect(res.statusCode).toBe(404);
  });

  it('should handle errors', async () => {
    const mockResponse = Promise.reject(unknownError);
    TodoModel.findByIdAndDelete.mockReturnValue(mockResponse);

    await TodoController.deleteTodo(req, res, next);
    expect(next).toHaveBeenCalledWith(unknownError);
  });

});
