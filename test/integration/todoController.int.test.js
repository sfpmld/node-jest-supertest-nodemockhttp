const request = require('supertest');
const app = require('../../app');

const newTodo = require('../mock-data/new-todo.json');
const updatedTodo = require('../mock-data/updated-todo.json');
const endPointUrl = '/todos/';
let firstTodo;
let todoCreated;
const fakeId = '111111111111111111111111';

describe(endPointUrl, () => {

  test(`GET ${endPointUrl}`, async () => {
    const response = await request(app)
      .get(endPointUrl);

    expect(response.status).toBe(201);
    expect(Array.isArray(response.body)).toBeTruthy();
    expect(response.body[0].title).toBeDefined();
    expect(response.body[0].done).toBeDefined();
    firstTodo = response.body[0];
  });

  test(`GET by Id ${endPointUrl}:id`, async () => {
    const response = await request(app)
      .get(endPointUrl + firstTodo._id);

    expect(response.status).toBe(200);
    expect(response.body._id).toEqual(firstTodo._id);
    expect(response.body.title).toEqual(firstTodo.title);
    expect(response.body.done).toEqual(firstTodo.done);
  });

  test(`GET by Id that doesn't exist ${endPointUrl}:id`, async () => {
    const response = await request(app)
      .get(endPointUrl + fakeId);

    expect(response.status).toBe(404);
  });

  test(`POST ${endPointUrl}`, async () => {
    const response = await request(app)
      .post(endPointUrl)
      .send(newTodo);

    expect(response.status).toBe(201);
    expect(response.body.title).toBe(newTodo.title);
    expect(response.body.done).toBe(newTodo.done);
    todoCreated = response.body;
  });

  it(`should return error 500 on malformed data with POST ${endPointUrl}`, async () => {
    const createTodo = {
      message: 'Done property missing',
    };
    const errorMocked = {
      message:
        'Todo validation failed: done: Path `done` is required., title: Path `title` is required.',
    };

    const response = await request(app)
      .post(endPointUrl)
      .send(createTodo);

    expect(response.status).toBe(500);
    expect(response.body).toEqual(errorMocked);
  });

  test(`PUT ${endPointUrl}`, async () => {
    const response = await request(app)
      .put(endPointUrl + todoCreated._id)
      .send(updatedTodo);

    expect(response.status).toBe(200);
    expect(response.body.title).toEqual(updatedTodo.title);
    expect(response.body.done).toEqual(updatedTodo.done);
  });

  test(`PUT by Id that doesn't exist ${endPointUrl}:id`, async () => {
    const response = await request(app)
      .put(endPointUrl + fakeId);

    expect(response.status).toBe(404);
  });

  test(`DELETE by Id ${endPointUrl}:id`, async () => {
    const response = await request(app)
      .delete(endPointUrl + firstTodo._id);

    expect(response.status).toBe(200);
    expect(response.body._id).toEqual(firstTodo._id);
    expect(response.body.title).toEqual(firstTodo.title);
    expect(response.body.done).toEqual(firstTodo.done);
  });

  test(`DELETE by Id that doesn't exist ${endPointUrl}:id`, async () => {
    const response = await request(app)
      .delete(endPointUrl + fakeId);

    expect(response.status).toBe(404);
  });

});
