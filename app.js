const express = require('express');
const app = express();
const mongodb = require('./mongodb/mongodbConnection');

const todoRoutes = require('./routes/todoRoutes');

// mongodb connect
mongodb.connect();

// json parser
app.use(express.json());
// routes
app.use('/todos', todoRoutes);

app.get('/', (_req, res) => {
  res.send('Hello world!');
});

// main error handling
app.use((error, _req, res, _next) => {
  const status = error.statusCode || 500;
  const message = error.message;
  res.status(status).json({
    message
  });
});


module.exports = app;
